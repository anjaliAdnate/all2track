this.dmap = {} || [];
(function (dmap) {

    dmap.map = function (div, latlng, zoom) {
        return new google.maps.Map(document.getElementById(div), {
            center: latlng,
            zoom: zoom
        });
    },
        dmap.mapCenter = function (map, latLng) {
            map.setCenter(latLng);
        };
    dmap.mark = function (map, myLatLng, title = '', image = null) {
        return new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: title,
            icon: image
        });
    };
    dmap.markMove = function (map, myLatLng, title = '', icons = null,label=null) {
        return new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: title,
            icon: icons,
            label: label
        });
    };
    dmap.markRm = function (mark) {
        return mark.setMap(null);
    };
    dmap.info = function (mark, cb) {
        mark.addListener('click', cb);
    };
    dmap.polyline = function (flightPlanCoordinates, strokeColor = '#FF0000', strokeOpacity = 1.0, strokeWeight = 2) {
        return new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: strokeColor,
            strokeOpacity: strokeOpacity,
            strokeWeight: strokeWeight
        });
    };
    dmap.polyGon = function (triangleCoords, strokeColor = '#FF0000', strokeOpacity = 0.8, strokeWeight = 2, fillColor = '#FF0000', fillOpacity = 0.35) {
        return new google.maps.Polygon({
            paths: paths,
            strokeColor: strokeColor,
            strokeOpacity: strokeOpacity,
            strokeWeight: strokeWeight,
            fillColor: fillColor,
            fillOpacity: fillOpacity
        });
    };
    dmap.getDistance = function (p1, p2) {
        var R = 6378137; // Earth’s mean radius in meter
        var dLat = (p2.lat - p1.lat) * Math.PI / 180;
        var dLong = (p2.lng - p1.lng) * Math.PI / 180
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos((p1.lat) * Math.PI / 180) * Math.cos((p2.lat) * Math.PI / 180) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d / 1000; // returns the distance in meter
    };
    dmap.flag = 'init';
    dmap.flag2 = 'init';

    dmap.play = function (map, mark, icons, coords, km_h = 50) {
        // this.playing=!this.playing;
        if (dmap.flag == 'init') {
            dmap.animateMarker(map, mark, icons, coords);
            dmap.flag = 'stop';

        } else
            if (dmap.flag == 'start') {
                dmap._moveMarker();
                dmap.flag = 'stop';
            } else
                if (dmap.flag == 'stop') {
                    //  dmap.speed = 0;
                    clearTimeout(this.start);
                    dmap.flag = 'start';
                }
        if (dmap.flag == 'reset') {
            mark.setPosition(coords[0]);
            dmap.flag = 'init';
        }
        return dmap.flag;
    };
    dmap.show = function () { console.log("show"); }
    dmap.init = function () {
        dmap.show();
    }
    dmap._goToPoint; dmap._moveMarker;
    dmap.speed = 50;
    dmap.delay = 10;
    dmap.marker;
    dmap.head=[0];

    dmap.speedInc = function (speed) {
        dmap.speed += speed;
    }
    dmap.speedDec = function (speed) {
        if(dmap.speed>50)
        dmap.speed -= speed;
    }
    dmap.animateMarker = function (map, mark, icons, coords) {
        var target = 0;
        dmap._goToPoint = function () {
            var lat = mark.position.lat();
            var lng = mark.position.lng();
            debugger;
            var step = (dmap.speed * 1000 * dmap.delay) / 3600000; // in meters
            var dest = new google.maps.LatLng(coords[target].lat, coords[target].lng);
            var distance = google.maps.geometry.spherical.computeDistanceBetween(dest, mark.position); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            //start remove code
            var speed = document.getElementById('showSpeed');
            speed.innerText ='speed='+coords[target].speed +' km/ph';
            var time = document.getElementById('showTime');
            time.innerText ='Time='+moment(coords[target].time).format('Do MMMM YYYY, h:mm a');
            //end remove code
            dmap._moveMarker = function () {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng))
                    icons.car.rotation=Math.floor(head/10)*10;
       
                    mark.setIcon(icons.car);
                    mark.setPosition(new google.maps.LatLng(lat, lng));
                    map.setCenter({ lat: lat, lng: lng });
                    dmap.start = setTimeout(dmap._moveMarker, dmap.delay);
                }
                else {
                    var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng));
                    icons.car.rotation=head;
                    mark.setIcon(icons.car);
                    mark.setPosition(dest);
                    map.setCenter({ lat: lat, lng: lng });
                    target++;
                    if (target == coords.length) {
                        // target = 0;
                        dmap.flag = 'reset';
                        clearTimeout(dmap.start);
                    }
                    dmap.start = setTimeout(dmap._goToPoint, dmap.delay);
                }
            }
            dmap._moveMarker();
        }
        dmap._goToPoint();
    }
    dmap.play2 = function (map, mark, icons, coords, km_h = 50) {
        // this.playing=!this.playing;
        dmap.speed = km_h;
        if (dmap.flag2 == 'init') {
            dmap.animateMarker2(map, mark, icons, coords);
            dmap.flag2 = 'stop';

        } else
            if (dmap.flag2 == 'start') {
                dmap._moveMarker2();
                dmap.flag2 = 'stop';
            } else
                if (dmap.flag2 == 'stop') {
                    //  dmap.speed = 0;
                    clearTimeout(this.start2);
                    dmap.flag2 = 'start';
                }
        if (dmap.flag2 == 'reset') {
            mark.setPosition(coords[0]);
            dmap.flag2 = 'init';
        }
        return dmap.flag2;
    };

    dmap.animateMarker2 = function (map, mark, icons, coords) {
        if(dmap.start2)
            clearTimeout(dmap.start2);
        var target = 0;
        dmap._goToPoint2 = function () {
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            console.log(dmap.speed);
            var step = (dmap.speed * 1000 * dmap.delay) / 3600000; // in meters
            var dest = new google.maps.LatLng(coords[target].lat, coords[target].lng);
            var distance = google.maps.geometry.spherical.computeDistanceBetween(dest, mark.position); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            // var speed = document.getElementById('showSpeed');
            // speed.innerText ='speed='+coords[target].speed +' km/ph';
            // var time = document.getElementById('showTime');
            // time.innerText ='Time='+moment(coords[target].time).format('Do MMMM YYYY, h:mm a');
            dmap._moveMarker2 = function () {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng))
                  //  icons.car.rotation=Math.floor(head/10)*10;
                  //console.log("if",head);
                //   let dltemp = document.querySelector("img[src='"+localStorage.icnUrl+"']");
                //   dltemp.style.transform = 'rotate('+head+'deg)';
                   // mark.setIcon(icons.car);
                   // mark.setIcon(dltemp);
                    mark.setPosition(new google.maps.LatLng(lat, lng));
                    map.setCenter({ lat: lat, lng: lng });
                    dmap.start2 = setTimeout(dmap._moveMarker2, dmap.delay);
                }
                else {
                    var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng));
                  //  icons.car.rotation=head;
                    //mark.setIcon(icons.car);
                    //console.log("else",head)
                    
                   // mark.setIcon(dltemp);

                   //////////////////////
                //    let dltemp = document.querySelector("img[src='"+localStorage.icnUrl+"']");
                //     dltemp.style.transform = 'rotate('+head+'deg)';
                    mark.setPosition(dest);
                    map.setCenter({ lat: lat, lng: lng });
                    target++;
                    if (target == coords.length) {
                        // target = 0;
                        dmap.flag2 = 'reset';
                        clearTimeout(dmap.start2);
                    }
                    dmap.start2 = setTimeout(dmap._goToPoint2, dmap.delay);
                }
            }
            dmap._moveMarker2();
        }
        dmap._goToPoint2();
    }
    dmap.liveTrack = function(map, mark, icons, coords,speed,delay,center=false) {
        var target = 0;
        function _goToPoint() {
            // dmap.speed = km_h;
            var lat = mark.position.lat();
            var lng = mark.position.lng();
            var step = (speed * 1000 * delay) / 3600000; // in meters
            var dest = new google.maps.LatLng(coords[target].lat, coords[target].lng);
            var distance = google.maps.geometry.spherical.computeDistanceBetween(dest, mark.position); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function _moveMarker () {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng))
                    if ((head != 0) || (head == NaN)) {
                        icons.rotation = head
                    }
                    mark.setIcon(icons);
                    if(center == true)
                    map.setCenter({ lat: lat, lng: lng });
                    mark.setPosition(new google.maps.LatLng(lat, lng));
                    setTimeout(_moveMarker,delay);
                }
                else {
                    var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        icons.rotation = head
                    }
                    mark.setIcon(icons);
                    if(center == true)
                    map.setCenter({ lat: lat, lng: lng });
                    mark.setPosition(new google.maps.LatLng(lat, lng));
                    target++;
                    setTimeout(_goToPoint,delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    }

    dmap.live= function (map, mark, icons,coords, km_h,delay) {
        this.map = map;
        this.mark = mark;
        this.icons = icons;
        this.coords =coords;
        this.speed = km_h;
        this.target = 0;
        this._goToPoint;
        this._moveMarker;
        this.delay=delay;
        this.start;
        this.flag='init';
            };
       
    
                dmap.live.prototype.runInc = function (speed) {
                    this.speed += speed;
                }
                dmap.live.prototype.runDec = function (speed) {
                    if(this.speed>50)
                        this.speed -= speed;
                }
                dmap.live.prototype.run=function (map,mark,icon,speed,delay){
                    this.target = 0;
                    this.speed = speed;
                    this.delay=delay;
                    this._goToPoint = function () {
                        var lat = mark.position.lat();
                        var lng = mark.position.lng();
                var step = (this.speed * 1000 * this.delay) / 3600000; // in meters
                var dest = new google.maps.LatLng(this.coords[this.target].lat, this.coords[this.target].lng);
                var distance = google.maps.geometry.spherical.computeDistanceBetween(dest, mark.position); // in meters
                var numStep = distance / step;
                var i = 0;
                var deltaLat = (this.coords[this.target].lat - lat) / numStep;
                var deltaLng = (this.coords[this.target].lng - lng) / numStep;
                this._moveMarker = function () {
                    lat += deltaLat;
                    lng += deltaLng;
                    i += step;
                    if (i < distance) {
                        var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng))
                        this.icons.rotation=Math.floor(head/10)*10;
            
                        mark.setIcon(this.icons.car);
                        mark.setPosition(new google.maps.LatLng(lat, lng));
                        map.setCenter({ lat: lat, lng: lng });
                        this.start = setTimeout(this._moveMarker, this.delay);
                    }
                    else {
                        var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng));
                        this.icons.rotation=head;
                        mark.setIcon(this.icons);
                        mark.setPosition(dest);
                        map.setCenter({ lat: lat, lng: lng });
                        this.target++;
                        if (this.target == this.coords.length) {
                            this.flag = 'reset';
                            clearTimeout(this.start);
                        }
                        this.start = setTimeout(this._goToPoint, this.delay);
                    }
                }
                this._moveMarker();
            }
            this._goToPoint();
        }

    return dmap;
})(this.dmap);